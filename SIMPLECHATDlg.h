// SIMPLECHATDlg.h : header file
//

#if !defined(AFX_SIMPLECHATDLG_H__D50B5DC0_518D_48E9_971F_D7FB00DB639D__INCLUDED_)
#define AFX_SIMPLECHATDLG_H__D50B5DC0_518D_48E9_971F_D7FB00DB639D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

/////////////////////////////////////////////////////////////////////////////
// CSIMPLECHATDlg dialog
#define RECEIVE_TIMER 1

class CSIMPLECHATDlg : public CDialog
{
	std::string m_Lines;

	// Construction
public:
	CSIMPLECHATDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	//{{AFX_DATA(CSIMPLECHATDlg)
	enum { IDD = IDD_SIMPLECHAT_DIALOG };
	CEdit	m_edtNick;
	CEdit	m_edtSay;
	CEdit	m_edtSpeak;
	CIPAddressCtrl	m_edtIP;
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSIMPLECHATDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	//{{AFX_MSG(CSIMPLECHATDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnButtonSend();
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnButtonListen();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SIMPLECHATDLG_H__D50B5DC0_518D_48E9_971F_D7FB00DB639D__INCLUDED_)
