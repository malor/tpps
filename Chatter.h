#if !defined(AFX_CHATTER_H__788B173F_A7B3_4B19_BE24_C6EE71565A4D__INCLUDED_)
#define AFX_CHATTER_H__788B173F_A7B3_4B19_BE24_C6EE71565A4D__INCLUDED_

const int LISTEN_PORT = 2000;
const int MAX_MSG_SIZE = 255;
const int MAX_MSGS_TO_REMEMBER = 100;
const int MAX_NAME_SIZE = 20;

struct tagMessage
{
	char szFrom[MAX_NAME_SIZE]; //nickname of sender
	char szText[MAX_MSG_SIZE]; //message text
};

class CChatter  
{
public:
	virtual ~CChatter();

	bool receiveMessage(tagMessage *pMsg);
	bool sendMessage(const char* pMsg);
	void connect( sockaddr_in sad, const char* szNick );
	bool isDataAvailable();

	static CChatter* getInstance();

private:
	CChatter();

	sockaddr_in m_ServerAddr;
	sockaddr_in m_LocalAddr;
	SOCKET m_Socket;
	char m_szNickName[MAX_NAME_SIZE];
};

#endif // !defined(AFX_CHATTER_H__788B173F_A7B3_4B19_BE24_C6EE71565A4D__INCLUDED_)
