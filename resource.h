//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by SIMPLECHAT.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_SIMPLECHAT_DIALOG           102
#define IDP_SOCKETS_INIT_FAILED         103
#define IDR_MAINFRAME                   128
#define IDC_IPADDRESS_PEER              1001
#define IDC_BUTTON_LISTEN               1002
#define IDC_BUTTON_CALL                 1003
#define IDC_EDIT_SAY                    1004
#define IDC_EDIT_SPEAK                  1005
#define IDC_BUTTON_SEND                 1006
#define IDC_BUTTON_DISCONECT            1007
#define IDC_EDIT_NICK                   1008

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        129
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1007
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
