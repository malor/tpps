#include "stdafx.h"

#include "SIMPLECHAT.h"
#include "SIMPLECHATDlg.h"
#include "Chatter.h"

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

	//{{AFX_DATA(CAboutDlg)
	enum { IDD = IDD_ABOUTBOX };
	//}}AFX_DATA

	//{{AFX_VIRTUAL(CAboutDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	//{{AFX_MSG(CAboutDlg)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
	//{{AFX_DATA_INIT(CAboutDlg)
	//}}AFX_DATA_INIT
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAboutDlg)
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
	//{{AFX_MSG_MAP(CAboutDlg)
		// No message handlers
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

CSIMPLECHATDlg::CSIMPLECHATDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CSIMPLECHATDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CSIMPLECHATDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	// Note that LoadIcon does not require a subsequent DestroyIcon in Win32
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CSIMPLECHATDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSIMPLECHATDlg)
	DDX_Control(pDX, IDC_EDIT_NICK, m_edtNick);
	DDX_Control(pDX, IDC_EDIT_SAY, m_edtSay);
	DDX_Control(pDX, IDC_EDIT_SPEAK, m_edtSpeak);
	DDX_Control(pDX, IDC_IPADDRESS_PEER, m_edtIP);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CSIMPLECHATDlg, CDialog)
	//{{AFX_MSG_MAP(CSIMPLECHATDlg)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_BUTTON_SEND, OnButtonSend)
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_BUTTON_LISTEN, OnButtonListen)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

BOOL CSIMPLECHATDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon
	
	SetTimer(RECEIVE_TIMER, 200, NULL);
	m_edtIP.SetWindowText(TEXT("127.0.0.1"));
	
	return TRUE;
}

void CSIMPLECHATDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

void CSIMPLECHATDlg::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

HCURSOR CSIMPLECHATDlg::OnQueryDragIcon()
{
	return (HCURSOR) m_hIcon;
}

void CSIMPLECHATDlg::OnButtonSend() 
{
    char sz[MAX_MSG_SIZE];
	m_edtSay.GetWindowText(sz, MAX_MSG_SIZE);
	VERIFY(CChatter::getInstance()->sendMessage(sz));
}

void CSIMPLECHATDlg::OnTimer(UINT nIDEvent) 
{
	if (nIDEvent != RECEIVE_TIMER) return;
	if (FALSE == CChatter::getInstance()->isDataAvailable()) return;
	tagMessage msg;
	CChatter::getInstance()->receiveMessage(&msg);
    m_Lines.append(msg.szFrom);
	m_Lines.append(":  ");
	m_Lines.append(msg.szText);
	m_Lines.append("\r\n");
	m_edtSpeak.SetWindowText(m_Lines.c_str());
	
	CDialog::OnTimer(nIDEvent);
}

void CSIMPLECHATDlg::OnButtonListen() 
{
    sockaddr_in sad;
    char szNick[MAX_NAME_SIZE];
	char szAddr[20];

	m_edtIP.GetWindowText(szAddr, 20);
	sad.sin_addr.s_addr = inet_addr(szAddr);
	m_edtNick.GetWindowText(szNick, MAX_NAME_SIZE);
	
	CChatter::getInstance()->connect(sad, szNick);
}