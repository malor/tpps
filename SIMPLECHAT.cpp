#include "stdafx.h"

#include "SIMPLECHAT.h"
#include "SIMPLECHATDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

CSIMPLECHATApp theApp;

BEGIN_MESSAGE_MAP(CSIMPLECHATApp, CWinApp)
	//{{AFX_MSG_MAP(CSIMPLECHATApp)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG
	ON_COMMAND(ID_HELP, CWinApp::OnHelp)
END_MESSAGE_MAP()

BOOL CSIMPLECHATApp::InitInstance()
{
	CSIMPLECHATDlg* dlg = new CSIMPLECHATDlg;
	m_pMainWnd = dlg;
	dlg->DoModal();
	return FALSE;
}