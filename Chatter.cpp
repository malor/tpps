#include "stdafx.h"

#include "Chatter.h"

CChatter* CChatter::getInstance()
{
	static CChatter chatter;
	return &chatter;
}

CChatter::CChatter()
{
	AfxSocketInit();

	m_Socket = socket(AF_INET, SOCK_DGRAM, PF_UNSPEC);
	if (m_Socket == INVALID_SOCKET)
	{
		MessageBox(NULL, "Socket creation failed !!", "Fatal Error", MB_OK);
	}

    m_LocalAddr.sin_family = AF_INET;
	m_LocalAddr.sin_port = htons(LISTEN_PORT);
	if (SOCKET_ERROR == bind(m_Socket, (sockaddr*)&m_LocalAddr, sizeof(m_LocalAddr))) 
	{
		MessageBox(NULL, "Socket bind failed !!", "Fatal Error", MB_OK);
        closesocket(m_Socket);
	}
}

CChatter::~CChatter()
{
	closesocket(m_Socket);
}

bool CChatter::isDataAvailable()
{
	fd_set fds;
    timeval tmout;
	tmout.tv_sec = 0;
	tmout.tv_usec = 1000;
    fds.fd_array[0] = m_Socket;
	fds.fd_count = 1;
    int i = select(1, &fds, NULL, NULL, &tmout);
	if ((i == SOCKET_ERROR) || (i == 0)) return FALSE;
	i = FD_ISSET(m_Socket, &fds);
	return (i != 0);	
}

void CChatter::connect(sockaddr_in sad, const char* szNick)
{
    m_ServerAddr = sad;
	m_ServerAddr.sin_port = htons(LISTEN_PORT);
	m_ServerAddr.sin_family = AF_INET;
	strncpy(m_szNickName, szNick, MAX_NAME_SIZE);
}

bool CChatter::sendMessage(const char* pMsg)
{
    tagMessage msg;
	memset(&msg, 0, sizeof(msg));
	strncpy(msg.szText, pMsg, MAX_MSG_SIZE);
	strncpy(msg.szFrom, m_szNickName, MAX_NAME_SIZE);
	return sendto(m_Socket, (char*)&msg, sizeof(tagMessage), 0, (sockaddr*)&m_ServerAddr, sizeof(m_ServerAddr));
}


bool CChatter::receiveMessage(tagMessage *pMsg)
{
	return recvfrom(m_Socket, (char*)pMsg, sizeof(tagMessage), 0, 0, 0) != SOCKET_ERROR;
}